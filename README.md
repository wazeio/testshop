# Shop cart API project #

## Installation ##
1. Install Node.js with npm
1. Install MongoDB
1. Run npm install -g pm2
1. Run cd to project dir
1. Run npm i
1. Change config/config.js if neccessary
1. Run mongod
1. Run pm2 start run.json

## Mocha tests ##
1. Run npm install -g mocha
1. Run cd to project dir
1. Run mocha