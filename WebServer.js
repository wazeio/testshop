'use strict';

const config = require('./config/config.js');
const db = require('./modules/mongoConnector.js');
const cookieSession = require('cookie-session');

const express = require('express');
const cors = require('cors');
const app = express();
const router = express.Router();
const bodyParser = require('body-parser');

// Общие настройки express + middleWares
app.use(bodyParser.urlencoded({     // URL-encoded bodies
    extended: true,
    parameterLimit: 10000,
    limit: 1024 * 1024 * 10,
}));
app.use(bodyParser.json({
    parameterLimit: 10000,
    limit: 1024 * 1024 * 10,
}));

app.use(cors());
app.use(express.static(__dirname + '/public'));

app.set('trust proxy', 1);
app.use(cookieSession({
    name: 'session',
    keys: ['secret1', 'secret2'],
}));
// Идентификация юзеров по сессиям
var sessionCounter = Date.now() * 1000;
app.use(function (req, res, next) {
    var newSession = req.session.isNew;
    console.log('New session: ' + newSession);
    if (newSession) {
        req.session.id = sessionCounter++;
    }
    next();
});
app.use('/', router);
// Роутер - контроллер
const controller = require('./controller');
// Проверка существования товаров в БД
function checkAndInsertProducts() {
    db.get('products').then(products => {
        if (products.length) {
            return;
        }
        var refProducts = require('./reference/products.js');
        if (!Array.isArray(refProducts) || !refProducts.length) {
            return console.error(new Error('Reference products unexpected error').stack);
        }
        return db.insert('products', refProducts);
    }).catch(err => console.error(err.stack));
}
// Инициализация БД и запуск сервера
db.init(config, err => {
    if (err) {
        return console.error(err.stack);
    }
    const initial = {
        db: db,
        app: app,
        router: router,
    };
    controller.init(initial);
    checkAndInsertProducts();
    app.listen(config.port || 3000);
});
