'use strict';

var db = {};
var logger = {
    fatal: function(s) {console.error(s)},
    error: function(s) {console.error(s)},
    warn: function(s) {console.error(s)},
    info: function(s) {console.log(s)},
    debug: function(s) {console.log(s)},
    trace: function(s) {console.trace(s)}
};

module.exports = {
    init: function(initial) {
        db = initial.db;
        logger = initial.logger || logger;
        return this;
    },
    get: function(params, callback) {
        if (typeof callback !== 'function') {
            return logger.error(new Error('Callback is not a function').stack);
        }
        var matchQuery = {$match: {}};
        var pipe = [
            matchQuery,
        ];
        var productId = params.productId;
        if (Array.isArray(productId)) {
            matchQuery.$match._id = {$in: productId};
        } else if (typeof productId === 'number') {
            matchQuery.$match._id = productId;
        }
        db.get('products', pipe).then(products => {
            for (let i=0; i<products.length; i++) {
                let product = products[i];
                product.id = product._id;
                delete product._id;
            }
            callback(null, {
                data: products,
            });
        }).catch(callback);
    },
};