'use strict';

var logger = {
    fatal: function(s) {console.error(s)},
    error: function(s) {console.error(s)},
    warn: function(s) {console.error(s)},
    info: function(s) {console.log(s)},
    debug: function(s) {console.log(s)},
    trace: function(s) {console.trace(s)}
};
var app = {};
var router = {};
var _initial = {};
var that = {};
var sessionCounter = 0;

const fs = require('fs');
const dir = './api/';
const fileNames = fs.readdirSync(dir);

// Подключение списка сущностей api
for (let i=0; i<fileNames.length; i++) {
    let fileName = fileNames[i];
    if (fileName.match(/\.js$/i) instanceof Array) {
        let key = fileName.replace(/\.js$/i, '');
        exports[key] = require('.' + dir + fileName);
        logger.debug('Add api entity: ' + key);
    }
}

function createRoutes() {
    var apiList = Object.keys(that);
    _initial.api = that;
    logger.debug('apiList: ' + apiList.toString());
    // Инициализация всех сущностей api
    for (let i=0; i<apiList.length; i++) {
        let apiName = apiList[i];
        if (apiName === 'init') {
            continue;
        }
        let api = that[apiName];
        if (typeof api !== 'object' || typeof api.init !== 'function') {
            continue;
        }
        api.init(_initial);
    }
    // Создание роутов на сущность
    // Получение списка
    router.get('/api/:entity', (req, res) => {
        logger.debug('Get GET request: ' + req.originalUrl);
        logger.debug(req.session);
        var params = req.params || {};

        var api = that[params.entity];
        if (typeof api !== 'object') {
            return blankException(req, res);
        }

        var method = api.get;
        if (typeof method !== 'function') {
            return blankException(req, res);
        }

        var data = req.query || {};
        data.sessionId = req.session.id;
        method(data, (err, result) => resultHandler(err, result, res));
    });
    // Получение данных
    router.post('/api/:entity', (req, res) => {
        logger.debug('Get GET request: ' + req.originalUrl);
        logger.debug(req.session);
        var params = req.params || {};

        var api = that[params.entity];
        if (typeof api !== 'object') {
            return blankException(req, res);
        }

        var method = api.put;
        if (typeof method !== 'function') {
            return blankException(req, res);
        }

        var data = req.body || {};
        data.sessionId = req.session.id;
        method(data, (err, result) => resultHandler(err, result, res));
    });
    // Удаление по id
    router.delete('/api/:entity/:id', (req, res) => {
        logger.debug('Get DELETE request: ' + req.originalUrl);
        logger.debug(req.session);
        var params = req.params || {};
        var api = that[params.entity];
        if (typeof api !== 'object') {
            return blankException(req, res);
        }
        var method = api.delete;
        if (typeof method !== 'function') {
            return blankException(req, res);
        }
        var id = params.id;
        method({
            id: id,
            sessionId: req.session.id,
        }, (err, result) => resultHandler(err, result, res));
    });
    // Обработчик ошибок сервера
    app.use(errorException);
    // Ошибка 404
    app.use(blankException);
}

function resultHandler(errList, result, res) {
    if (Array.isArray(errList) && errList.length) {
        return res.status(400).send({
            error: {
                params: errList,
                type: "invalid_param_error",
                message: "Invalid data parameters"
            }
        });
    }
    res.json(result);
}
function errorException(err, req, res, next) {
    logger.error(err.stack);
    res.status(500).send({
        error: {
            type: 'internal_server_error',
            message: err.toString(),
        }
    });
}
function blankException(req, res, next) {
    res.status(404).send({
        error: {
            type: 'invalid_request_error',
            message: 'Unable to resolve the request "' + req.originalUrl + '".',
        }
    });
}

exports.init = function(initial) {
    that = this;
    router = initial.router;
    app = initial.app;
    logger = initial.logger || logger;
    _initial = initial;
    // Создание роутов
    createRoutes();
};