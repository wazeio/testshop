'use strict';

var mongoClient = require('mongodb').MongoClient;
var logger = {
    fatal: function(s) {console.error(s)},
    error: function(s) {console.error(s)},
    warn: function(s) {console.error(s)},
    info: function(s) {console.log(s)},
    debug: function(s) {console.log(s)},
    trace: function(s) {console.trace(s)}
};
var config = {};
var db = {};

function connect(url, callback) {
    mongoClient.connect(url, function(err, _db) {
        if (err) {
            console.error('MongoDB connection error: ' + err.toString() + ' (Try to reconnect after 1 second)');
            setTimeout(connect, 1000, url, callback);
            return;
        }
        if (typeof callback === 'function') {
            callback(err, _db);
        }
    });
}
function checkCollections(collections, callback) {
    collections = collections || config.mongodb.tables;
    var promises = [];
    for (let i=0; i<collections.length; i++) {
        promises.push(checkCollection(collections[i]));
    }
    return Promise.all(promises);
}
function checkCollection(collection) {
    return new Promise((resolve, reject) => {
        db.createCollection(collection, err => {
            if (err) {
                console.error('Creating connection ' + collection + ' failed: ' +  err.toString());
                reject(err);
            }
            else {
                console.info('Creating connection ' + collection + ' status: success');
                resolve(true);
            }
        });
    });
}

module.exports = {
    init: function(params, callback) {
        if (typeof callback !== 'function') {
            return logger.error(new Error('Callback is not a function').stack);
        }
        config = params;
        // Пробуем сгенерировать url для подключения к БД
        try {
            var url = 'mongodb://' + config.mongodb.host + ':' + config.mongodb.port + '/' + config.mongodb.path;
        } catch(err) {
            return callback(err);
        }
        // Подключаемся
        connect(url, (err, _db) => {
            if (err) {
                logger.error('Connection to MongoDB failed', err);
                return callback(err);
            } else {
                logger.info('Connected correctly to server');
                db = _db;
                checkCollections().then(results => {
                    logger.info('DB collections checked');
                    callback(null, results);
                }).catch(callback);
            }
        });
    },
    get: function(collectionName, params, callback) {
        callback = typeof callback === 'function' ? callback : function(){};
        logger.debug('Try to get from ' + collectionName + ' with query: ' + JSON.stringify(params));
        return new Promise((resolve, reject) => {
            var collection = db.collection(collectionName);
            var result = [];
            var stream = collection.aggregate(params).stream();
            stream.on('data', doc => {
                result.push(doc);
            });
            stream.on('error', err => {
                reject(err);
                callback(err);
            });
            stream.on('end', () => {
                stream.close();
                logger.debug('Get data from ' + collectionName + ': ' + JSON.stringify(result));
                resolve(result);
                callback(null, result);
            });
        });
    },
    insert: function(collectionName, data, callback) {
        callback = typeof callback === 'function' ? callback : function(){};
        return new Promise((resolve, reject) => {
            if (typeof data !== 'object') {
                return logger.error(new Error('Data type is unexpected: ' + typeof data).stack);
            }
            if (!Array.isArray(data)) {
                data = [data];
            }
            var collection = db.collection(collectionName);
            collection.insertMany(data, (err, result) => {
                if (err) {
                    reject(err);
                    return callback(err);
                }
                resolve(result);
                callback(null, result);
            });
        });
    },
    destroy: function(collectionName, data, callback) {
        callback = typeof callback === 'function' ? callback : function(){};
        return new Promise((resolve, reject) => {
            data = data || {};
            var collection = db.collection(collectionName);
            collection.deleteMany(data, (err, result) => {
                if (err) {
                    reject(err);
                    return callback(err);
                }
                resolve(result);
                callback(null, result);
            });
        });
    }
};