module.exports = [
    {
        _id: 1,
        name: "Milk",
        description: "Product Description",
        price: 300,
    },
    {
        _id: 2,
        name: "Water",
        description: "Product Description",
        price: 100,
    },
    {
        _id: 3,
        name: "Bread",
        description: "Product Description",
        price: 87,
    },
    {
        _id: 4,
        name: "Butter",
        description: "Product Description",
        price: 150,
    },
    {
        _id: 5,
        name: "Cat",
        description: "Product Description",
        price: 1500,
    },
];