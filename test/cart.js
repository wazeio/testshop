// Use mocha for tests
'use strict';

const cart = require('../api/cart.js');
const products = require('../api/products.js');
const db = require('../modules/mongoConnector.js');
// TODO Replace assert by chai
const assert = require('assert');
// Подготовка среды для тестирования
const config = require('../config/config.js') || {};
config.mongodb.path = 'test';
var initial = {};
// Проверка существования товаров в БД
function insertProductsIntoDB(done) {
    db.destroy('products', {}).then(() => {
        var refProducts = require('../reference/products.js');
        if (!Array.isArray(refProducts) || !refProducts.length) {
            throw new Error('Reference products unexpected error');
        }
        return db.insert('products', refProducts);
    }).then(result => done()).catch(done);
}
function init(done) {
    db.init(config, err => {
        if (err) {
            throw err;
        }
        initial = {
            db: db,
            api: {
                products: products,
                cart: cart,
            }
        };
        products.init(initial);
        insertProductsIntoDB(done);
    });
}

describe('Test cart api', () => {
    before(done => {
        init(done);
    });

    it('Cart initialization', done => {
        var result = cart.init(initial);
        assert.equal(typeof result, 'object');
        done();
    });

    it('Get cart data', done => {
        cart.get({
            sessionId: 12345,
        }, (err, result) => {
            assert.equal(err, null);
            assert.deepEqual(result, {
                total_sum: 0,
                products_count: 0,
                products: [],
            });
            done();
        });
    });
    // TODO: Add tests for put, delete and more test for get after
});